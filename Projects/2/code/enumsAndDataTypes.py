from __future__ import annotations
from enum import Enum
from copy import deepcopy


##### Utility functions
class BetterBytes(bytes):
    """
    Extension of the bytes class, adds some useful methods
    """

    @staticmethod
    def nullfill(bytestring: bytes, length: int) -> bytes:
        if length <= len(bytestring):
            return b'' + bytestring
        return (length-len(bytestring)) * b"\x00" + bytestring

    @staticmethod
    def extract_n_bits(bytestring: bytes, bit_len: int, bit_offset: int) -> bytes:
        """
        Sets the bits in the range [bit_offset:bit_offset+bit_len] to 0
        """
        byte_len = -(bit_len // (-8))
        bytes_to_modify = bytestring[:byte_len]
        mask = ((1<<((byte_len<<3)-bit_len))-1) >> bit_offset
        intsum = int.from_bytes(bytes_to_modify, "big") & mask
        return intsum.to_bytes(byte_len, "big") + bytestring[byte_len:]

def aggregate(encodable_iterable) -> int:
    """
    Packs bits of the elements of an iterable objet into a single int

    :param encodable_iterable: an iterable over objects that have an encode_to_int() function
    that returns the object's bits in the form of an int
    """
    res = 0
    to_shift = 0
    for el in reversed(encodable_iterable):
        res += el.encode_to_int() << to_shift
        to_shift += len(el)
    return res


##### CUSTOM DATATYPES #####
class IntWrapper:
    """
    Enforces the lenght of an int and adds useful methods
    """
    val: int
    bit_length: int

    def __init__(self, val, bit_length) -> None:
        assert val < (1<<bit_length)
        self.val = val
        self.bit_length = bit_length

    def __len__(self) -> int:
        return self.bit_length

    def encode_to_int(self) -> int:
        return self.val

    def __hash__(self) -> int:
        return hash(self.val)

    def __eq__(self, other) -> bool:
        return self.val==other.val and self.bit_length==other.bit_length

    def __repr__(self) -> str:
        return f"Int{self.bit_length}={self.val}"

    def __str__(self) -> str:
        return str(self.val)


class Bool(IntWrapper):
    def __init__(self, val: bool | int) -> None:
        super().__init__(int(bool(val)), 1)

    @staticmethod
    def __len__() -> int:
        return 1

    @staticmethod
    def from_bytes(bytestring: bytes, offset: int = 0) -> Bool:
        return Bool(bytestring[0]>>(7-offset))


class Int3(IntWrapper):
    def __init__(self, val: int) -> None:
        super().__init__(int(val), 3)

    @staticmethod
    def __len__() -> int:
        return 3

    @staticmethod
    def from_bytes(bytestring: bytes, offset: int = 0) -> Int3:
        return Int3(bytestring[0]>>(5-offset))

    def __iadd__(self, other: int | Int3) -> Int3:
        if type(other) == Int3:
            self.val += other.val
        elif type(other) == int:
            self.val += other
        return self

class Int4(IntWrapper):
    def __init__(self, val: int) -> None:
        super().__init__(int(val), 4)

    @staticmethod
    def __len__() -> int:
        return 4

    @staticmethod
    def from_bytes(bytestring: bytes, offset: int = 0) -> Int4:
        return Int4(bytestring[0]>>(4-offset))

    def __iadd__(self, other: int | Int4) -> Int4:
        if type(other) == Int4:
            self.val += other.val
        elif type(other) == int:
            self.val += other
        return self

class Int8(IntWrapper):
    def __init__(self, val: int) -> None:
        super().__init__(int(val), 8)

    @staticmethod
    def __len__() -> int:
        return 8

    @staticmethod
    def from_bytes(bytestring: bytes, offset: int = 0) -> Int8:
        return Int8(bytestring[0])

    def __iadd__(self, other: int | Int8) -> Int8:
        if type(other) == Int8:
            self.val += other.val
        elif type(other) == int:
            self.val += other
        return self

    def to_bytes(self) -> bytes:
        return self.val.to_bytes(len(self), "big")

class Int16(IntWrapper):
    def __init__(self, val: int) -> None:
        super().__init__(int(val), 16)

    @staticmethod
    def __len__() -> int:
        return 16

    @staticmethod
    def from_bytes(bytestring: bytes, offset: int = 0) -> Int16:
        return Int16(int.from_bytes(bytestring[:2], "big"))

    def __iadd__(self, other: int | Int16) -> Int16:
        if type(other) == Int16:
            self.val += other.val
        elif type(other) == int:
            self.val += other
        return self

    # def to_bytes(self) -> bytes:
        # return BetterBytes.from_int(self.val, len(self))

class Int32(IntWrapper):
    def __init__(self, val: int) -> None:
        super().__init__(int(val), 32)

    @staticmethod
    def __len__() -> int:
        return 32

    @staticmethod
    def from_bytes(bytestring: bytes, offset: int = 0) -> Int32:
        return Int32(int.from_bytes(bytestring[:4], "big"))

    def __iadd__(self, other: int | Int32) -> Int32:
        if type(other) == Int32:
            self.val += other.val
        elif type(other) == int:
            self.val += other
        return self

    def to_bytes(self) -> bytes:
        return self.val.to_bytes(len(self), "big")

class ListOfInt8:
    """
    Represents a generic list of bytes, encoded as istances of the Int8 class
    """
    list_: list[Int8]
    compressed_length: int
    type_of_data: int # 0->raw, 1->IPv4, 2->IPv6, 3->Hostname

    def __init__(self, hostname: str = "", type_of_data: int = 0) -> None:
        self.list_ = []
        self.type_of_data = type_of_data

        if hostname != "":
            for part in hostname.split('.'):
                self.list_.append(Int8(len(part)))
                for ch in part:
                    self.list_.append(Int8(ord(ch)))

            self.list_.append(Int8(0))

        self.compressed_length = 0

    def __len__(self) -> int:
        if self.compressed_length == 0:
            self.compressed_length = len(self.list_)
        return self.compressed_length

    def __add__(self, other: ListOfInt8) -> ListOfInt8:
        to_ret = ListOfInt8()
        to_ret.list_ = self.list_ + other.list_
        return to_ret

    def __iadd__(self, other: ListOfInt8) -> ListOfInt8:
        self.list_ += other.list_
        return self

    def __str__(self) -> str:
        """
        Depending on the type of the data (specified at construction), returns a string representation
        """
        # Raw data -> print exadecimal representation
        if self.type_of_data == 0:
            return ' '.join(i.to_bytes().hex() for i in self.list_)

        # IPv4
        elif self.type_of_data == 1:
            return '.'.join(str(i.val) for i in self.list_)

        # IPv6
        elif self.type_of_data == 2:
            return ':'.join(hex((self.list_[i].val<<8) + self.list_[i+1].val)[2:] for i in range(0, len(self.list_), 2))

        # Hostname
        elif self.type_of_data == 3:
            to_ret = ""
            index = 0
            while True:
                n_ch = self.list_[index].val
                index += 1

                if n_ch == 0:
                    break

                for i in range(index, index+n_ch):
                    to_ret += self.list_[i].val.to_bytes(1, "big").decode()
                    index += 1

                to_ret += "."

            return to_ret
        else:
            raise RuntimeError

    def append(self, n: Int8 | int) -> ListOfInt8:
        if type(n) == Int8:
            self.list_.append(deepcopy(n))
        elif type(n) == int:
            self.list_.append(Int8(n))
        else:
            raise RuntimeError
        return self

    @staticmethod
    def from_bytes(bytestring: bytes, type_of_data: int, full_bytestring: bytes, n_el: int = 1<<17) -> ListOfInt8:
        data = ListOfInt8(type_of_data=type_of_data)
        index = 0
        bit_len = 0

        if type_of_data != 3:
            while index < n_el:
                data.append(bytestring[index])
                index += 1
                bit_len += 8

        else:
            # Apply format for compressed data
            while True:
                n_ch = bytestring[index]
                data.append(n_ch)
                bit_len += 8
                index += 1

                if n_ch == 0:
                    break

                # If compressed bits are set
                elif n_ch & 0b11000000:
                    addr = int.from_bytes(bytestring[index-1:index+1], "big") & 0b0011111111111111
                    data.list_.pop()
                    bit_len += 8
                    data += ListOfInt8.from_bytes(full_bytestring[addr:], type_of_data, full_bytestring, n_el-index-1)
                    break

                else:
                    for _ in range(n_ch):
                        data.append(bytestring[index])
                        index += 1
                        bit_len += 8
                        if index == n_el:
                            break

        data.compressed_length = bit_len
        b = str(data)
        return data

    def to_bytes(self) -> bytes:
        return b''.join(i.val.to_bytes(1, "big") for i in self.list_)


###### Enums ######
class IntWrapperEnum(Enum):
    def __len__(self) -> int:
        return len(self.value[0])

    def encode_to_int(self) -> int:
        return self.value[0].val


class Type(IntWrapperEnum):  #16bit entry
    ERROR = Int16(0),
    A = Int16(1),
    NS = Int16(2),
    MD = Int16(3),
    MF = Int16(4),
    CNAME = Int16(5),
    SOA = Int16(6),
    MB = Int16(7),
    MG = Int16(8),
    MR = Int16(9),
    NULL = Int16(10),
    WKS = Int16(11),
    PTS = Int16(12),
    HINFO = Int16(13),
    MINFO = Int16(14),
    MX = Int16(15),
    TXT = Int16(16),
    AAAA = Int16(28),

    @staticmethod
    def __len__() -> int:
        return 16

    @staticmethod
    def from_bytes(bytestring: bytes, offset: int = 0) -> Type:
        return Type((Int16(int.from_bytes(bytestring[:2], "big")), ))

    def to_bytes(self) -> bytes:
        return self.value[0].val.to_bytes(2, "big")


class Qtype(IntWrapperEnum): #16-bit
    ERROR = Int16(0),
    A = Int16(1),
    NS = Int16(2),
    MD = Int16(3),
    MF = Int16(4),
    CNAME = Int16(5),
    SOA = Int16(6),
    MB = Int16(7),
    MG = Int16(8),
    MR = Int16(9),
    NULL = Int16(10),
    WKS = Int16(11),
    PTS = Int16(12),
    HINFO = Int16(13),
    MINFO = Int16(14),
    MX = Int16(15),
    TXT = Int16(16),
    AXFR = Int16(252),
    MAILB = Int16(253),
    MAILA = Int16(254),
    ALL = Int16(255),

    @staticmethod
    def __len__() -> int:
        return 16

    @staticmethod
    def from_bytes(bytestring: bytes, offset: int = 0) -> Qtype:
        return Qtype((Int16(int.from_bytes(bytestring[:2], "big")), ))

    def to_bytes(self) -> bytes:
        return self.value[0].val.to_bytes(2, "big")

class Class(IntWrapperEnum): #16-bit
    IN = Int16(1),
    CS = Int16(2),
    CH = Int16(3),
    HS = Int16(4),

    @staticmethod
    def __len__() -> int:
        return 16

    @staticmethod
    def from_bytes(bytestring: bytes, offset: int = 0) -> Class:
        return Class((Int16(int.from_bytes(bytestring[:2], "big")), ))

    def to_bytes(self) -> bytes:
        return self.value[0].val.to_bytes(2, "big")

class QClass(IntWrapperEnum): #16-bit
    IN = Int16(1),
    CS = Int16(2),
    CH = Int16(3),
    HS = Int16(4),
    ANY = Int16(255),

    @staticmethod
    def __len__() -> int:
        return 16

    @staticmethod
    def from_bytes(bytestring: bytes, offset: int = 0) -> QClass:
        return QClass((Int16(int.from_bytes(bytestring[:2], "big")), ))

    def to_bytes(self) -> bytes:
        return self.value[0].val.to_bytes(2, "big")

class Opcode(IntWrapperEnum): #4-bit
    QUERY = Int4(0),
    IQUERY = Int4(1),
    STATUS = Int4(2),
    # NOTIFY = 4,
    # UPDATE = 5,
    # DSO = 6,

    @staticmethod
    def __len__() -> int:
        return 4

    @staticmethod
    def from_bytes(bytestring: bytes, offset: int = 0) -> Opcode:
        return Opcode((Int4(bytestring[0] >> (4-offset)), ))

class Rcode(IntWrapperEnum): #4-bit
    NOERR = Int4(0),
    FORMAT_ERR = Int4(1),
    SERVER_FAIL = Int4(2),
    NAME_ERROR = Int4(3),
    NOT_IMPLEM = Int4(4),
    REFUSED = Int4(5),

    @staticmethod
    def __len__() -> int:
        return 4

    @staticmethod
    def from_bytes(bytestring: bytes, offset: int = 0) -> Rcode:
        return Rcode((Int4(bytestring[0] >> (4-offset)), ))


