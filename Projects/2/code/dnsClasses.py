from __future__ import annotations
import random
from dataclasses import dataclass, field
from enumsAndDataTypes import *

class DNSmessage:

    @dataclass
    class DNSmessageHeader:
        """
            DNS header
                                          1  1  1  1  1  1
            0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
            |                      ID                       |
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
            |QR|   Opcode  |AA|TC|RD|RA|   Z    |   RCODE   |
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
            |                    QDCOUNT                    |
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
            |                    ANCOUNT                    |
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
            |                    NSCOUNT                    |
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
            |                    ARCOUNT                    |
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
        """

        ID:      Int16  = field(default_factory=lambda: Int16(0))
        QR:      Bool   = field(default_factory=lambda: Bool(False))
        Opcode:  Opcode = field(default_factory=lambda: Opcode.QUERY) #4bit
        AA:      Bool   = field(default_factory=lambda: Bool(0))
        TC:      Bool   = field(default_factory=lambda: Bool(0))
        RD:      Bool   = field(default_factory=lambda: Bool(0))
        RA:      Bool   = field(default_factory=lambda: Bool(0))
        Z:       Int3   = field(default_factory=lambda: Int3(0)) #always 0
        RCODE:   Rcode  = field(default_factory=lambda: Rcode.NOERR) #4bit
        QDCOUNT: Int16  = field(default_factory=lambda: Int16(0))
        ANCOUNT: Int16  = field(default_factory=lambda: Int16(0))
        NSCOUNT: Int16  = field(default_factory=lambda: Int16(0))
        ARCOUNT: Int16  = field(default_factory=lambda: Int16(0))

        @staticmethod
        def __len__() -> int:
            """returns the lenght of the header in bits"""
            return 96

        def to_bytes(self) -> bytes:
            int_sum = aggregate(vars(self).values())
            return int_sum.to_bytes(len(self)//8, "big")

        @staticmethod
        def from_bytes(bytestring: bytes) -> DNSmessage.DNSmessageHeader:
            to_ret = DNSmessage.DNSmessageHeader()
            bit_counter = 0
            for prop_name, prop_value in vars(to_ret).items():
                parsed_object = prop_value.from_bytes(bytestring, bit_counter)

                # Count the bit offset inside a single byte
                bit_counter += len(parsed_object)
                setattr(to_ret, prop_name, parsed_object)

                # Remove extracted bytes from bytestring
                bytestring = BetterBytes.extract_n_bits(bytestring, len(parsed_object), bit_counter-len(parsed_object))[bit_counter//8:]
                bit_counter %= 8
            return to_ret

    @dataclass
    class DNSmessageQuestion:
        """
            DNS question
                                          1  1  1  1  1  1
            0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
            |                                               |
            /                     QNAME                     /
            /                                               /
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
            |                     QTYPE                     |
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
            |                     QCLASS                    |
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
        """

        QNAME: ListOfInt8     = field(default_factory=lambda: ListOfInt8())
        QTYPE: Qtype          = field(default_factory=lambda: Qtype.A)   #16bit
        QCLASS: QClass        = field(default_factory=lambda: QClass.IN) #16bit, =1 for Internet

        def __len__(self) -> int:
            """returns the lenght of the question in bits"""
            return len(self.QNAME) + 32

        def to_bytes(self) -> bytes:
            to_ret = b''
            for i in vars(self).values():
                to_ret += i.to_bytes()
            return to_ret

        @staticmethod
        def from_bytes(bytestring: bytes, full_bytestring: bytes, offset: int) -> DNSmessage.DNSmessageQuestion:
            to_ret = DNSmessage.DNSmessageQuestion()
            bit_counter = 0
            for prop_name, prop_value in vars(to_ret).items():
                if prop_name == "QNAME":
                    parsed_object = prop_value.from_bytes(bytestring, 3, full_bytestring)
                else:
                    parsed_object = prop_value.from_bytes(bytestring, bit_counter)
                bit_counter += len(parsed_object)
                setattr(to_ret, prop_name, parsed_object)
                bytestring = BetterBytes.extract_n_bits(bytestring, len(parsed_object), bit_counter-len(parsed_object))[bit_counter//8:]
                bit_counter %= 8
            return to_ret

    @dataclass
    class DNSmessageResource:
        """
            DNS resource, can be one of {response, authority, additional}
                                          1  1  1  1  1  1
            0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
            |                                               |
            /                                               /
            /                      NAME                     /
            |                                               |
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
            |                      TYPE                     |
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
            |                     CLASS                     |
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
            |                      TTL                      |
            |                                               |
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
            |                   RDLENGTH                    |
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--|
            /                     RDATA                     /
            /                                               /
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
        """

        # Members of DNSmessageResource
        NAME:     ListOfInt8     = field(default_factory=lambda: ListOfInt8())
        TYPE:     Type           = field(default_factory=lambda: Type.A) #2*8bit
        CLASS:    Int16          = field(default_factory=lambda: Int16(0)) #2*8bit
        TTL:      Int32          = field(default_factory=lambda: Int32(0))
        RDLENGHT: Int16          = field(default_factory=lambda: Int16(0))
        RDATA:    ListOfInt8     = field(default_factory=lambda: ListOfInt8())

        def __len__(self) -> int:
            """returns the lenght of the resource in bits"""
            return len(self.NAME) + 80 + self.RDLENGHT.val*8

        def to_bytes(self) -> bytes:
            return sum(i.to_bytes() for i in vars(self).values())

    @dataclass
    class DNSmessageResponse(DNSmessageResource):

        @staticmethod
        def from_bytes(bytestring: bytes, full_bytestring: bytes, offset: int) -> DNSmessage.DNSmessageResponse:
            to_ret = DNSmessage.DNSmessageResponse()
            bit_counter = 0
            for prop_name, prop_value in vars(to_ret).items():
                if prop_name == "NAME":
                    parsed_object = prop_value.from_bytes(bytestring, 3, full_bytestring)
                elif prop_name == "RDATA":
                    if to_ret.TYPE == Type.A:
                        parsed_object = prop_value.from_bytes(bytestring, 1, full_bytestring, to_ret.RDLENGHT.val)
                    elif to_ret.TYPE == Type.AAAA:
                        parsed_object = prop_value.from_bytes(bytestring, 2, full_bytestring, to_ret.RDLENGHT.val)
                    else:
                        raise RuntimeError
                else:
                    parsed_object = prop_value.from_bytes(bytestring, bit_counter)

                bit_counter += len(parsed_object)
                setattr(to_ret, prop_name, parsed_object)
                bytestring = BetterBytes.extract_n_bits(bytestring, len(parsed_object), bit_counter-len(parsed_object))[bit_counter//8:]
                bit_counter %= 8
            return to_ret

    @dataclass
    class DNSmessageAuthority(DNSmessageResource):

        @staticmethod
        def from_bytes(bytestring: bytes, full_bytestring: bytes, offset: int) -> DNSmessage.DNSmessageAuthority:
            to_ret = DNSmessage.DNSmessageAuthority()
            bit_counter = 0
            for prop_name, prop_value in vars(to_ret).items():
                if prop_name == "NAME":
                    parsed_object = prop_value.from_bytes(bytestring, 3, full_bytestring)
                elif prop_name == "RDATA":
                    if to_ret.TYPE == Type.A:
                        parsed_object = prop_value.from_bytes(bytestring, 1, full_bytestring, to_ret.RDLENGHT.val)
                    elif to_ret.TYPE == Type.AAAA:
                        parsed_object = prop_value.from_bytes(bytestring, 2, full_bytestring, to_ret.RDLENGHT.val)
                    elif to_ret.TYPE == Type.NS:
                        parsed_object = prop_value.from_bytes(bytestring, 3, full_bytestring, to_ret.RDLENGHT.val)
                    else:
                        raise RuntimeError
                else:
                    parsed_object = prop_value.from_bytes(bytestring, bit_counter)
                # print(f"{prop_name}: {parsed_object}")
                bit_counter += len(parsed_object)
                setattr(to_ret, prop_name, parsed_object)
                bytestring = BetterBytes.extract_n_bits(bytestring, len(parsed_object), bit_counter-len(parsed_object))[bit_counter//8:]
                bit_counter %= 8
            return to_ret


    @dataclass
    class DNSmessageAdditional(DNSmessageResource):

        @staticmethod
        def from_bytes(bytestring: bytes, full_bytestring: bytes, offset: int) -> DNSmessage.DNSmessageAdditional:
            to_ret = DNSmessage.DNSmessageAdditional()
            bit_counter = 0
            for prop_name, prop_value in vars(to_ret).items():
                if prop_name == "NAME":
                    parsed_object = prop_value.from_bytes(bytestring, 3, full_bytestring)
                elif prop_name == "RDATA":
                    if to_ret.TYPE == Type.A:
                        parsed_object = prop_value.from_bytes(bytestring, 1, full_bytestring, to_ret.RDLENGHT.val)
                    elif to_ret.TYPE == Type.AAAA:
                        parsed_object = prop_value.from_bytes(bytestring, 2, full_bytestring, to_ret.RDLENGHT.val)
                    else:
                        raise RuntimeError
                else:
                    parsed_object = prop_value.from_bytes(bytestring, bit_counter)
                # print(f"{prop_name}: {parsed_object}")
                bit_counter += len(parsed_object)
                setattr(to_ret, prop_name, parsed_object)
                bytestring = BetterBytes.extract_n_bits(bytestring, len(parsed_object), bit_counter-len(parsed_object))[bit_counter//8:]
                bit_counter %= 8
            return to_ret

    """
        DNSmessage
        +---------------------+
        |        Header       |
        +---------------------+
        |       Question      | the question for the name server
        +---------------------+
        |        Answer       | RRs answering the question
        +---------------------+
        |      Authority      | RRs pointing toward an authority
        +---------------------+
        |      Additional     | RRs holding additional information
        +---------------------+
    """

    # Members of DNSmessage
    header: DNSmessageHeader
    questions: list[DNSmessageQuestion]
    responses: list[DNSmessageResponse]
    authorities: list[DNSmessageAuthority]
    additionals: list[DNSmessageAdditional]
    full_bytestring: bytes



    def __init__(self, hostname: str = "", QR: bool = False, RD: bool = False, transactionID: int = 0) -> None:
        """
            init method that constructs the header for a message, non-specified fields are initialized to 0

            :param hostname: the fully qualified hostname
            :param QR: False for query, True for response
            :param RD: Recursion Desired
            :param transactionID: transactionID for the message
        """

        self.header = self.DNSmessageHeader(
                            ID = Int16(transactionID),
                            QR = Bool(QR),
                            Opcode = Opcode.QUERY,
                            RD = Bool(RD),
                        )

        self.questions = []
        self.responses = []
        self.authorities = []
        self.additionals = []
        self.full_bytestring = b''

    def add_section(self, section: DNSmessageHeader | DNSmessageQuestion | DNSmessageResource) -> None:
        if type(section) == self.DNSmessageHeader:
            self.header = section

        elif type(section) == self.DNSmessageQuestion:
            self.questions.append(section)
            self.header.QDCOUNT += 1

        elif type(section) == self.DNSmessageResponse:
            self.responses.append(section)
            self.header.ANCOUNT += 1

        elif type(section) == self.DNSmessageAuthority:
            self.authorities.append(section)
            self.header.NSCOUNT += 1

        elif type(section) == self.DNSmessageAdditional:
            self.additionals.append(section)
            self.header.ARCOUNT += 1

    def get_bytestring(self) -> bytes:
        """returns the bit-encoding of the message in the form of a bytestring"""
        prop_list = [self.header] + self.questions + self.responses + self.authorities + self.additionals
        to_ret = b''
        for i in prop_list:
            to_ret += i.to_bytes()
        return to_ret


    @staticmethod
    def from_bytes(bytestring: bytes) -> DNSmessage:
        to_ret = DNSmessage()
        to_ret.full_bytestring = bytestring
        offset = 0

        # Parse header
        header = DNSmessage.DNSmessageHeader.from_bytes(bytestring)
        to_ret.add_section(header)
        bytestring = bytestring[len(header)//8:]
        offset += len(header)

        # Parse Question(s)
        for _ in range(header.QDCOUNT.val):
            question = DNSmessage.DNSmessageQuestion.from_bytes(bytestring, to_ret.full_bytestring, offset)
            to_ret.add_section(question)
            bytestring = bytestring[len(question)//8:]
            offset += len(question)

        # Parse Response(s)
        for _ in range(header.ANCOUNT.val):
            response = DNSmessage.DNSmessageResponse.from_bytes(bytestring, to_ret.full_bytestring, offset)
            to_ret.add_section(response)
            bytestring = bytestring[len(response)//8:]
            offset += len(response)

        # Parse Authority(s)
        for _ in range(header.NSCOUNT.val):
            authority = DNSmessage.DNSmessageAuthority.from_bytes(bytestring, to_ret.full_bytestring, offset)
            to_ret.add_section(authority)
            bytestring = bytestring[len(authority)//8:]
            offset += len(authority)

        # Parse Additional(s)
        for _ in range(header.ARCOUNT.val):
            additional = DNSmessage.DNSmessageAuthority.from_bytes(bytestring, to_ret.full_bytestring, offset)
            to_ret.add_section(additional)
            bytestring = bytestring[len(additional)//8:]
            offset += len(additional)

        return to_ret



class DNSquery(DNSmessage):
    def __init__(self, hostname, transactionID: int = random.randint(0, 1<<16-1), recursion: bool = True) -> None:

        super().__init__(hostname, QR=False, RD=recursion, transactionID=transactionID)

        question = super().DNSmessageQuestion(
                        QNAME = ListOfInt8(hostname),
                        QTYPE = Qtype.A,
                        QCLASS = QClass.IN
                    )

        super().add_section(question)
