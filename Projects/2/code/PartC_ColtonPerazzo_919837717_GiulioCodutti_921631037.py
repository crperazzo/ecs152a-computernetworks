import socket
import time
from dnsClasses import *

websiteURLs = ["facebook.com", "tmz.com", "nytimes.com", "cnn.com"]

root_servers = {
    "a.root-servers.net": "198.41.0.4",
    "b.root-servers.net": "199.9.14.201",
    "c.root-servers.net": "192.33.4.12",
    "d.root-servers.net": "199.7.91.13",
    "e.root-servers.net": "192.203.230.10",
    "f.root-servers.net": "192.5.5.241",
    "g.root-servers.net": "192.112.36.4",
    "h.root-servers.net": "198.97.190.53",
    "i.root-servers.net": "192.36.148.17",
    "j.root-servers.net": "192.58.128.30",
    "k.root-servers.net": "193.0.14.129",
    "l.root-servers.net": "199.7.83.42",
    "m.root-servers.net": "202.12.27.33",
}

# Format:
#    {hostname0 : (IP0, TTL0, TimeCached)
#       ...
#    }
cached_IPs: dict[str, tuple[str, int, float]] = dict()


def send_bytes(UDPsock: socket.socket, bytes_to_send: bytes, IP: str, port: int) -> tuple[bytes, int]:

    UDPsock.sendto(bytes_to_send, (IP, port))

    start = time.perf_counter()
    bytes_recieved = UDPsock.recvfrom(4096)[0]
    RTT = time.perf_counter() - start

    return (bytes_recieved, int(RTT*1000))


def resolveIP(hostname: str, debug_print: bool = False) -> tuple[str, int]:

    if hostname in cached_IPs:
        cached_IP, TTL, time_cached = cached_IPs[hostname]
        time_now = time.time()
        if time_cached + TTL > time_now:
            return cached_IP, int(TTL - time_now + time_cached)

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0) as UDPClientSocket:
        UDPClientSocket.settimeout(10.0)

        # resolve host
        for root_name, root_IP in root_servers.items():
            if debug_print: print(f"Querying server {root_name} ({root_IP})")

            query = DNSquery(hostname, recursion=False)
            bytes_to_root = query.get_bytestring()

            try:

                bytes_from_root, RTT = send_bytes(UDPClientSocket, bytes_to_root, root_IP, 53)

                if debug_print: print(f"  Got response in {RTT}ms")

                root_response = DNSmessage.from_bytes(bytes_from_root)

                for TLD in root_response.authorities:
                    if TLD.TYPE == Type.A:
                        if debug_print: print(f"\n  Trying TLD {TLD.NAME} ({TLD.RDATA}): ")
                        bytes_to_TLD = DNSquery(hostname, recursion=False).get_bytestring()

                        try:
                            TLD_IP = str(TLD.RDATA)
                            bytes_from_TLD, RTT = send_bytes(UDPClientSocket, bytes_to_TLD, TLD_IP, 53)

                            if debug_print: print(f"    Got response in {RTT}ms")

                            TLDrespose = DNSmessage.from_bytes(bytes_from_TLD)

                            for auth in TLDrespose.authorities:
                                if auth.TYPE == Type.A:
                                    if debug_print: print(f"\n    Trying Authority {auth.NAME} ({auth.RDATA}): ")
                                    bytes_to_auth = DNSquery(hostname, recursion=False).get_bytestring()

                                    try:
                                        auth_IP = str(auth.RDATA)
                                        bytes_from_auth, RTT = send_bytes(UDPClientSocket, bytes_to_auth, auth_IP, 53)

                                        if debug_print: print(f"      Got response in {RTT}ms")

                                        auth_respose = DNSmessage.from_bytes(bytes_from_auth)

                                        for IP in auth_respose.responses:
                                            if IP.TYPE == Type.A:
                                                if debug_print: print(f"      Got IP for {hostname}: {IP.RDATA}\n")

                                                resolvedIP = str(IP.RDATA)

                                                cached_IPs[hostname] = (resolvedIP, IP.TTL.val, time.time())
                                                return (resolvedIP, IP.TTL.val)

                                    except socket.timeout:
                                        if debug_print: print("      connection timeout, trying another server if available")
                        except socket.timeout:
                            if debug_print: print("    connection timeout, trying another server if available")
            except socket.timeout:
                if debug_print: print("  connection timeout, trying another server if available")

        raise RuntimeError("Impossible to resolve host")


if __name__ == "__main__":
    print("First run with empty cache:")
    for websiteURL in websiteURLs:
        print(f"  --{websiteURL}--")
        start = time.perf_counter()
        IP, TTL = resolveIP(websiteURL, debug_print=False)
        RTT = time.perf_counter() - start

        print(f"    Got IP for {websiteURL} in {RTT*1000:.0f}ms: {IP} (TTL={TTL}s)\n")

    print("Second run with cache populated:")
    for websiteURL in websiteURLs:
        print(f"  --{websiteURL}--")
        start = time.perf_counter()
        IP, TTL = resolveIP(websiteURL, debug_print=False)
        RTT = time.perf_counter() - start

        print(f"    Got IP for {websiteURL} in {RTT*1000:.0f}ms: {IP} (TTL={TTL}s)\n")

