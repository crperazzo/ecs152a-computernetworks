import socket
import time
import sys
from dnsClasses import *

DEBUG = False
hostname = "tmz.com"

root_servers = {
    "a.root-servers.net": "198.41.0.4",
    "b.root-servers.net": "199.9.14.201",
    "c.root-servers.net": "192.33.4.12",
    "d.root-servers.net": "199.7.91.13",
    "e.root-servers.net": "192.203.230.10",
    "f.root-servers.net": "192.5.5.241",
    "g.root-servers.net": "192.112.36.4",
    "h.root-servers.net": "198.97.190.53",
    "i.root-servers.net": "192.36.148.17",
    "j.root-servers.net": "192.58.128.30",
    "k.root-servers.net": "193.0.14.129",
    "l.root-servers.net": "199.7.83.42",
    "m.root-servers.net": "202.12.27.33",
}

def resolveIP(url: str, debug_print: bool = False) -> list[str]:

    servers: list[str] = ["", "", "", ""] #Root, TLD, auth, host

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0) as UDPClientSocket:

        UDPClientSocket.settimeout(10.0)

        # resolve host
        for root_name, root_IP in root_servers.items():
            if debug_print: print(f"Querying server {root_name} ({root_IP})")

            query = DNSquery(url, recursion=False)
            bytes_to_send = query.get_bytestring()

            try:
                UDPClientSocket.sendto(bytes_to_send, (root_IP, 53))

                start = time.perf_counter()
                bytes_from_root = UDPClientSocket.recvfrom(4096)[0]
                RTT = time.perf_counter() - start

                if debug_print: print(f"  Got response in {RTT*1000:.0f}ms")

                root_response = DNSmessage.from_bytes(bytes_from_root)

                for TLD in root_response.authorities:
                    if TLD.TYPE == Type.A:
                        if debug_print: print(f"\n  Trying TLD {TLD.NAME} ({TLD.RDATA}): ")

                        servers[1] = str(TLD.RDATA)

                        bytes_to_send = DNSquery(url, recursion=False).get_bytestring()

                        try:
                            UDPClientSocket.sendto(bytes_to_send, (str(TLD.RDATA), 53))

                            start = time.perf_counter()
                            bytes_from_TLD = UDPClientSocket.recvfrom(4096)[0]
                            RTT = time.perf_counter() - start

                            if debug_print: print(f"    Got response in {RTT*1000:.0f}ms")

                            TLDrespose = DNSmessage.from_bytes(bytes_from_TLD)

                            for auth in TLDrespose.authorities:
                                if auth.TYPE == Type.A:
                                    if debug_print: print(f"\n    Trying Authority {auth.NAME} ({auth.RDATA}): ")

                                    servers[2] = str(auth.RDATA)

                                    bytes_to_send = DNSquery(url, recursion=False).get_bytestring()

                                    try:
                                        UDPClientSocket.sendto(bytes_to_send, (str(auth.RDATA), 53))

                                        start = time.perf_counter()
                                        bytes_from_auth = UDPClientSocket.recvfrom(4096)[0]
                                        RTT = time.perf_counter() - start

                                        if debug_print: print(f"      Got response in {RTT*1000:.0f}ms")

                                        auth_respose = DNSmessage.from_bytes(bytes_from_auth)

                                        for IP in auth_respose.responses:
                                            if IP.TYPE == Type.A:
                                                if debug_print: print(f"      Got IP for {url}: {IP.RDATA}\n")

                                                servers[3] = str(auth.RDATA)

                                                return servers

                                    except socket.timeout:
                                        if debug_print: print("      connection timeout, trying another server if available")
                        except socket.timeout:
                            if debug_print: print("    connection timeout, trying another server if available")
            except socket.timeout:
                if debug_print: print("  connection timeout, trying another server if available")

    raise RuntimeError("Impossible to resolve host")

if __name__ == "__main__":

    if "--debug" in sys.argv:
        DEBUG = True
        sys.argv.remove("--debug")

    if len(sys.argv)>1:
        hostname = sys.argv[1]

    print(f"Domain: {hostname}")

    levels_IPs = resolveIP(hostname, debug_print=DEBUG)

    for name, IP in zip(["Root", "TLD", "Authoritative", "HTTP"], levels_IPs):
        print(f"{name} server IP address: {IP}")

