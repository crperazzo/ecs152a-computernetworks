# Report


### Group Information
1. Colton Perazzo
    - Student ID: 919837717
    - Discussion Group: A01
2. Giulio Codutti
    - Student ID: 921631037
    - Discussion Group: A02

## Part A
### A.1 Question #1
Describe in detail the DNS request and response header format in your implementation.

In our implementation (according to  RFC 1035, Section 4), the DNS request and response headers can be found in ```dnsClasses.py```. We used the class ```DNSquery``` to generate our request
while the response is parsed and stored in the class ```DNSmessage```.

As described in RFC 1035, Section 4:
```
                    1  1  1  1  1  1
  0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
  |                                               |
  /                     QNAME                     /
  /                                               /
  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
  |                     QTYPE                     |
  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
  |                     QCLASS                    |
  +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
```
The request header format is as shown above (from class ```DNSmessageHeader```), this header is used to carry our "request" in the query and contains which parameters we defined of what is being asked. ```QNAME``` refers to the domain name represented as a sequence of labels. ```QTYPE``` refers to a two octet code that specifices the type of the query being asked, and lastly ```QCLASS``` refers to the class of the query. Inside the ```DNSquery``` class our header is contained which has a length of the first ```96``` bits and the length of the class ```DNSmessageQuestion``` (as explained above).

In regards to the response and the class ```DNSmessageHeader```, 
```
                  1  1  1  1  1  1
0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
|                      ID                       |
+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
|QR|   Opcode  |AA|TC|RD|RA|   Z    |   RCODE   |
+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
|                    QDCOUNT                    |
+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
|                    ANCOUNT                    |
+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
|                    NSCOUNT                    |
+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
|                    ARCOUNT                    |
+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
```
The response header format is shown above (taken from ROFC 1035, section 4). ```ID``` refers to a 16-bit identifier that is assigned by the program to specify the query. ```QR``` refers to a bit field that imples if it is a query *(0)* or a response *(1)*. ```OPCODE``` is a 4-bit identifier that defines what kind of query is contained in the message. ```AA``` is the authoritative answer, ```TC``` is the truncation, ```RD``` is if recursion is desired, ```RA``` is if recusion is avilable, ```Z``` is if this is reserved for future use and lastly ```RCODE``` is the response code. 

```QDCOUNT```, ```ANCOUNT```, ```NSCOUNT```, and ```ARCOUNT``` are all 16-bit integers specifying the number of entires, resource records, server resource records, and additonal resource records in the question, answer, authority record, and additional record sections.

In ```dnsClasses.py```, we have a class labeled ```DNSmessage``` that holds all the seralized data for a message. 

### A.2 Question #2
Compute the RTT between your DNS client to each of the public DNS resolvers. Do you
notice any meaningful differences across different DNS resolvers? Explain.

The RTT between the DNS client and each of the public DNS resolvers can be achieved by initalizing a start variable to ```start = time.perf_counter()``` to record the float value of time in seconds before we call 
```UDPClientSocket.recvfrom()```. ```UDPClientSocket.recvform()``` receives a message from the socket and captures the address from which the data was sent too. If successful, we can caluate RTT after the ```UDPClientSocket.recvform()``` call which is then equal to ```RTT = time.perf_counter() - start```.

```
Iran servers
 querying server 91.245.229.1
  recieved IPs in 249 ms: 10.10.34.35

output from code/PartA_ColtonPerazzo_919837717_GiulioCodutti_921631037.py (using the --debug flag)
```
**Iran RTT = 249ms**

```
USA servers
 querying server 169.237.229.88
  recieved IPs in 10 ms: 18.154.144.81, 18.154.144.120, 18.154.144.128, 18.154.144.76

output from code/PartA_ColtonPerazzo_919837717_GiulioCodutti_921631037.py (using the --debug flag)
```
**USA RTT = 10ms**

```
Canada servers
 querying server 136.159.85.15
  recieved IPs in 50 ms: 18.67.39.19, 18.67.39.37, 18.67.39.84, 18.67.39.42

output from code/PartA_ColtonPerazzo_919837717_GiulioCodutti_921631037.py (using the --debug flag)
```
**Canada RTT = 50ms**

As you can see from the data provided above, there is a noticable difference in the RTT values between our DNS client and each of the public DNS resolvers. As expected, the USA had the fastest RTT with ```10ms``` followed by Canada with ```50ms``` and then Iran with ```249ms```. An explanation for the meaninful difference is simply **propagation delay** (proportional to distance), the length that our signal has to travel correlates with the round trip time for our DNS client request to reach the DNS resolver and for the response to reach back to our DNS client. Since we are currently located in the USA, the round trip time is the smallest as we are closest to these servers. Iran on the other hand is around ~7,200 miles away from the USA- resulting in a slower round trip time.

### A.3 Question #3
Compute the RTT between your HTTP client to the HTTP server of the resolved
hostname.

Similar to the question above, the RTT between the HTTP client to the HTTP server can be calculated by first initalzing a start variable to record the float value of time in seconds before the request is called. After the value is recorded we can then call ```client.recv()```. In a nutshell the ```recv()``` function essentially does the same as the ```recvfrom()``` function however it does not capture the address from which the data was sent too. If successful, we can calculate the RTT between our HTTP client and the HTTP server of the resolved hostname in the same process as the previous question.

```
Using 18.67.39.19 for the request

HTTP server IP address: 18.67.39.19
Sending GET request
Request recieved in 66ms

output from code/PartA_ColtonPerazzo_919837717_GiulioCodutti_921631037.py (using the --debug flag)
```
**RTT = 66ms**

As shown from the data provided above, the RTT between the HTTP client and the HTTP server is **66ms**.

## Part B
### B.1 Question #1
Compute the RTT from your local DNS server to each of the DNS servers including the
root name server, the TLD name server, and the authoritative DNS server of tmz.com.

*Note: The process for obtaining the RTT* ```RTT = time.perf_counter() - start``` *is the same for this question as the previous part*.
```
Domain: tmz.com
Querying server a.root-servers.net (198.41.0.4)
  Got response in 51ms

  Trying TLD e.gtld-servers.net. (192.12.94.30):
    Got response in 35ms

    Trying Authority ns-385.awsdns-48.com. (205.251.193.129):
      Got response in 27ms
      Got IP for tmz.com: 13.227.74.28

Root server IP address: 198.41.0.4
TLD server IP address: 192.12.94.30
Authoritative server IP address: 205.251.193.129
HTTP server IP address: 205.251.193.129

output from code/PartB_ColtonPerazzo_919837717_GiulioCodutti_921631037.py (using the --debug flag)
```

The root name server has an RTT of ```51ms```, the TLD name server has a RTT of ```35ms``` and lastly the authoriative DNS server has a RTT of ```27ms```.

## Part C
### C.1 Question #1
Report the time it takes to resolve each of these host names from your local DNS server.

```
First run with empty cache:
  --facebook.com--
    Got IP for facebook.com in 89ms: 157.240.22.35

  --tmz.com--
    Got IP for tmz.com in 99ms: 13.227.74.47

  --nytimes.com--
    Got IP for nytimes.com in 92ms: 151.101.1.164

  --cnn.com--
    Got IP for cnn.com in 213ms: 151.101.195.5

output from code/PartC_ColtonPerazzo_919837717_GiulioCodutti_921631037.py
```

With an ampty cache, *facebook.com* resolves in ```89ms```. *tmz.com* resolves in ```99ms```, *nytimes.com* resolves in ```92ms```, and lastly *cnn.com* resolves in ```213ms```.

### C.2 Question #2
Report the TTL value in the DNS responses to each of these host names.

```
First run with empty cache:
  --facebook.com--
    Got IP for facebook.com in 89ms: 157.240.22.35 (TTL=300s)

  --tmz.com--
    Got IP for tmz.com in 99ms: 13.227.74.47 (TTL=60s)

  --nytimes.com--
    Got IP for nytimes.com in 92ms: 151.101.1.164 (TTL=120s)

  --cnn.com--
    Got IP for cnn.com in 213ms: 151.101.195.5 (TTL=60s)

output from code/PartC_ColtonPerazzo_919837717_GiulioCodutti_921631037.py
```

*facebook.com* has a TTL of ```300s```. *tmz.com* has a TTL of ```60s```, *nytimes.com* has a TTL of ```120s```, and lastly *cnn.com* has a TTL of ```60s```.

### C.3 Question #3
Report the time it takes to resolve each of these host names by your DNS client from
your local DNS server when it did implement the cache (and the answers are already in
the cache).

```
Second run with cache populated:
  --facebook.com--
    Got IP for facebook.com in 0ms: 157.240.22.35 (TTL=299s)

  --tmz.com--
    Got IP for tmz.com in 0ms: 13.227.74.47 (TTL=59s)

  --nytimes.com--
    Got IP for nytimes.com in 0ms: 151.101.1.164 (TTL=119s)

  --cnn.com--
    Got IP for cnn.com in 0ms: 151.101.195.5 (TTL=59s)

output from code/PartC_ColtonPerazzo_919837717_GiulioCodutti_921631037.py
```

With the cached IPs, all of the host names (*facebook.com*, *tmz.com*, *nytimes.com*, *cnn.com*) are resolved in ```0ms``` as predicited since the cached included the answers.
