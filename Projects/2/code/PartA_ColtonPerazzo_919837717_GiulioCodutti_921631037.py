import socket
import time
from dnsClasses import *
import sys

DEBUG = False
hostname = "tmz.com"
dump_name = "http_dump.txt"

servers = {
    "Iran": ("91.245.229.1", "46.224.1.42", "185.161.112.34"),
    "USA": ("169.237.229.88", "168.62.214.68", "104.42.159.98"),
    "Canada": ("136.159.85.15", "184.94.80.170", "142.103.1.1")
}

if __name__ == "__main__":

    if "--debug" in sys.argv:
        DEBUG = True
        sys.argv.remove("--debug")

    if len(sys.argv)>1:
        hostname = sys.argv[1]

    print(f"Domain: {hostname}")

    # Create a socket object and set timeout
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0) as UDPClientSocket:

        UDPClientSocket.settimeout(10.0)

        resolvedIP = ""

        for server_name, serverIPs in servers.items():
            if DEBUG: print(f"{server_name} servers")

            for serverIP in serverIPs:
                if DEBUG: print(f" querying server {serverIP}")

                try:
                    query = DNSquery(hostname, recursion=True, transactionID=97)
                    bytes_to_send = query.get_bytestring()

                    UDPClientSocket.sendto(bytes_to_send, (serverIP, 53))

                    start = time.perf_counter()
                    bytesFromServer = UDPClientSocket.recvfrom(4096)[0]
                    RTT = time.perf_counter() - start

                    responseFromServer = DNSmessage.from_bytes(bytesFromServer)
                    IPs = list(str(response.RDATA) for response in responseFromServer.responses)
                    if DEBUG: print(f"  recieved IPs in {RTT*1000:.0f} ms:", ', '.join(IPs))

                    try:
                        resolvedIP = IPs[0]
                    except:
                        raise RuntimeError("Got no IP from the servers")

                    break

                except socket.timeout:
                    if DEBUG: print("  connection timeout, trying another server if available")

            if DEBUG: print()



    if DEBUG: print(f"Using {resolvedIP} for the request\n")

    print(f"HTTP server IP address: {resolvedIP}")

    # Initiate TCP connection (handshake) and create GET request
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as serverSocket:

        serverSocket.connect((resolvedIP,80))
        request = f"GET / HTTP/1.1\r\nHost:{hostname}\r\n\r\n"

        if DEBUG: print("\nSending GET request")

        serverSocket.send(request.encode())

        start = time.perf_counter()
        response = serverSocket.recv(4096)
        RTT = time.perf_counter() - start

        if DEBUG: print(f"Request recieved in {RTT*1000:.0f}ms")

        with open(dump_name, "wb") as out:
            out.write(response)

        if DEBUG: print("Written html response dump")

