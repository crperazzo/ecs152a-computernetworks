# Report #2


### 2.1 Information
1. Colton Perazzo
    - Student ID: 919837717
    - Discussion Group: A01
2. Giulio Codutti
    - Student ID: 921631037
    - Discussion Group: A02

Code files submitted:
1. code/part2/pcap.py

Pcap files submitted:
1. project1_part2.pcap

### 2.2 Question #1
How many devices are connected to this hotspot?

There are **4** devices connected to this hotspot.

```
---Ex1---
  4

output from code/part2/pcap.py
```

### 2.3 Question #2
Which device sends out the most number of packets?

The most packets were sent out by the device with an ip address of **10.42.0.52**.

```
---Ex2---
  10.42.0.52

output from code/part2/pcap.py
```
### 2.4 Question #3
Which device receives the most number of packets?

The devices that receives the most packets has an ip address of **10.42.0.149**.

```
---Ex3---
  10.42.0.149

output from code/part2/pcap.py
```

### 2.5 Question #4
Is there any endpoint where more than one device sends out a network packet to it? List
the IP addresses of these endpoint(s)?

```
---Ex4---
  142.250.189.227
  10.42.0.1
  142.250.189.164
  142.251.214.138
  157.240.22.35
  157.240.22.25
  142.250.189.195
  142.250.189.174
  172.217.164.110
  142.250.189.193
  142.250.191.74
  142.250.191.66
  142.250.189.226
  142.251.46.225
  142.250.189.206
  13.227.74.104
  142.251.46.163
  13.227.74.42
  142.250.72.195
  142.250.189.200
  63.140.36.137
  34.120.253.250
  34.111.8.32
  13.227.74.54
  35.190.60.146
  142.251.32.46
  34.98.72.95
  74.119.118.134
  104.244.42.67
  142.250.72.194
  142.250.189.166
  13.227.74.80
  142.251.46.238
  142.251.32.42
  74.119.118.149
  142.250.72.202
  13.227.74.99
  142.251.46.173
  142.250.191.46
  172.217.164.106
  142.250.72.206
  142.250.189.182
  104.254.148.251
  34.98.64.218
  104.36.113.35
  18.214.193.123
  198.54.12.127
  74.119.118.138
  192.40.34.237
  35.212.133.238
  23.73.128.31
  104.36.113.24
  13.107.42.14
  142.251.2.188
  142.250.189.238
  142.250.189.234
  142.250.189.162
  142.251.46.162
  151.101.194.133
  13.227.74.90
  104.244.42.197
  104.18.18.126
  151.101.194.217
  13.227.74.5
  13.227.74.72
  107.21.60.168
  142.251.46.161
  8.39.36.194
  35.190.90.30
  35.227.197.177
  104.18.41.98
  104.244.42.129
  152.199.24.185
  104.244.42.66
  146.75.92.158
  104.244.42.195
  104.123.154.187
  152.199.24.163
  13.227.78.100
  146.75.92.159
  23.205.204.244

output from code/part2/pcap.py
```

### 2.6 Question #5
Which application layer protocol has been used the most by the devices?

HTTP was used the most with **182940** uses, followed by DNS with **2396**
and HTTP with **496**.

| HTTPS       | DNS    | HTTP |
| -----       | ---    | ---- |
| 182940      | 2396   | 496  |

```
 ---Ex5---
  https: 182940
  dns: 2396
  http: 496

output from code/part2/pcap.py
```

### 2.7 Question #6
Identify how much time did it take for us to capture this Pcap file (in minutes).

It took **8 minutes and 56 seconds** to full capture this pcap file.

```
 ---Ex6---
  8:56 sec

output from code/part2/pcap.py
```

### 2.8 Question #7
Can you tell whether the devices send packets concurrently or sequentially? Explain
your approach to figure the sets of devices with concurrent/sequential network traffic.
List the sets of devices with concurrent traffic.

Our approach for this problem was to compare the devices that communicate at the
same exact timestamp. If a device sends a packet with a timestamp exactly the same
as the timestamp of a packet from another device, then it is running on a
concurrent network where devices use the network at the same time, otherwise
it is a sequential network where the packets are sent one after another.

Devices with concurrent traffic:
1. 10.42.0.149
2. 10.42.0.32
3. 10.42.0.52

Devices with sequential traffic:
1. 10.42.0.193

```
 ---Ex7---
  10.42.0.149, 10.42.0.32, 10.42.0.52

output from code/part2/pcap.py
```

### 2.9 Question #8
Can you figure out at approximately what point of time the devices were disconnected
from the hotspot?

1. 10.42.0.32
    - Last IP request: **7 minutes and 49 seconds**
    - Last ARP response: **7 minutes and 44 seconds**
2. 10.42.0.52
    - Last IP request: **8 minutes and 9 seconds**
    - Last ARP response: **7 minutes and 51 seconds**
3. 10.42.0.149
    - Last IP request: **8 minutes and 14 seconds**
    - Last ARP response: **7 minutes and 52 seconds**
4. 10.42.0.139
    - Last IP request: **8 minutes and 17 seconds**
    - Last ARP response: **8 minutes and 12 seconds**

```
 ---Ex8---
 Last IP request sent:
  10.42.0.32: 7:49 sec
  10.42.0.52: 8:09 sec
  10.42.0.149: 8:14 sec
  10.42.0.193: 8:17 sec
 Last ARP response sent:
  22188226e256<=>10.42.0.32: 7:44 sec
  a87eea6730d6<=>10.42.0.149: 7:51 sec
  f8e61a6d5f89<=>10.42.0.52: 7:52 sec
  d4c94ba0c8a2<=>10.42.0.193: 8:12 sec

output from code/part2/pcap.py
```