# Report #1


### 1.1 Information
1. Colton Perazzo
    - Student ID: 919837717
    - Discussion Group: A01
2. Giulio Codutti
    - Student ID: 921631037
    - Discussion Group: A02

Code files submitted:
1. code/part1/har.py (this file makes use of the external [haralyzer](https://github.com/haralyzer/haralyzer) library, and has been used only to compare the results that we got with a manual analysis using Google's [HAR Analyzer](https://toolbox.googleapps.com/apps/har_analyzer/))
2. code/part1/pcap.py

Pcap files submitted:
1. youtube.pcap
2. videolan.pcap
3. peertube.pcap
4. etrigan.pcap
5. tmz.pcap

Har files submitted:
1. www.youtube.com_redacted.har
2. www.videolan.org_redacted.har
3. www.joinpeertube.org_redacted.har
4. www.etrigannews.com_redacted.har
5. www.tmz.com_redacted.har

### 1.2 Question #1
How many UDP and TCP packets did you observe for each website?

| Link         | UDP Packets | TCP Packets |
| -----------  | ----------- | ----------- |
| youtube      | 24421       | 101         |
| videolan     | 74          | 2757        |
| peertube     | 552         | 10812       |
| etrigan      | 2230        | 28723       |
| tmz          | 42770       | 10031       |

```
www.youtube.com
---Ex1---
  UDP: 24421, TCP: 101

www.videolan.org
---Ex1---
  UDP: 74, TCP: 2757

www.joinpeertube.org
---Ex1---
  UDP: 552, TCP: 10812

www.etrigannews.com
---Ex1---
  UDP: 2230, TCP: 28723

www.tmz.com
---Ex1---
  UDP: 42770, TCP: 10031

output from code/part1/pcap.py
```

### 1.3 Question #2
How much network traffic (number of packets sent) is secure (HTTPS) vs
vulnerable (HTTP) on each site?

In this exercise, we realized that all of the websites reported **0** HTTP connections.
It's possible that the used browser (Firefox 105.0.3) blocks HTTP connections by default.

Har files (without background noise- more accurate)
| Link         | HTTP | HTTPS   |
| -----------  | ---- | -----   |
| youtube      | 0    | 242     |
| videolan     | 0    | 348     |
| peertube     | 0    | 247     |
| etrigan      | 0    | 49      |
| tmz          | 0    | 653     |

Pcap files (with background noise):
| Link         | HTTP | HTTPS     |
| -----------  | ---- | -----     |
| youtube      | 0    | 24494     |
| videolan     | 0    | 2815      |
| peertube     | 0    | 11177     |
| etrigan      | 0    | 30758     |
| tmz          | 0    | 52625     |


```
www.youtube.com
---Ex2---
  HTTP reqs: 0, HTTPS reqs: 242

www.videolan.org
---Ex2---
  HTTP reqs: 0, HTTPS reqs: 348

www.joinpeertube.org
---Ex2---
  HTTP reqs: 0, HTTPS reqs: 247

www.etrigannews.com
---Ex2---
  HTTP reqs: 0, HTTPS reqs: 49

www.tmz.com
---Ex2---
  HTTP reqs: 0, HTTPS reqs: 653

output from code/part1/har.py
```

```
www.youtube.com
---Ex2---
  HTTP reqs: 0, HTTPS reqs: 24494

www.videolan.org
---Ex2---
  HTTP reqs: 0, HTTPS reqs: 2815

www.joinpeertube.org
---Ex2---
  HTTP reqs: 0, HTTPS reqs: 11177

www.etrigannews.com
---Ex2---
  HTTP reqs: 0, HTTPS reqs: 30758

www.tmz.com
---Ex2---
  HTTP reqs: 0, HTTPS reqs: 52625

output from code/part1/pcap.py
```

### 1.4 Question #3
What is the distribution of different types of packets that you observed for each
site? Calculate and report the percentage of packets observed for HTTP, HTTPS,
DNS, FTP, SSH, DHCP, TELNET, SMTP, POP3, and NTP.

For this exercise, we used the total number of requests from the pcap files
(with background noise).
1. www.youtube.com
    - HTTP: 0/24522 = 0%
    - HTTPS: 24494/24522 = 99.89%
    - DNS: 28/24522 = 0.11%
    - FTP: 0/24522 = 0%
    - SSH: 0/24522 = 0%
    - DHCP: 0/24522 = 0%
    - TELNET: 0/24522 = 0%
    - SMTP: 0/24522 = 0%
    - POP3: 0/24522 = 0%
    - NTP: 0/24522 = 0%

2. www.videolan.org
    - HTTP: 0/2831 = 0%
    - HTTPS: 2815/2831 = 99.43%
    - DNS: 16/2831 = 0.57%
    - FTP: 0/2831 = 0%
    - SSH: 0/2831 = 0%
    - DHCP: 0/2831 = 0%
    - TELNET: 0/2831 = 0%
    - SMTP: 0/2831 = 0%
    - POP3: 0/2831 = 0%
    - NTP: 0/2831 = 0%

3. www.joinpeertube.org
    - HTTP: 0/11364 = 0%
    - HTTPS: 11177/11364 = 98.35%
    - DNS: 65/11364 = 0.57%
    - FTP: 0/11364 = 0%
    - SSH: 0/11364 = 0%
    - DHCP: 0/11364 = 0%
    - TELNET: 0/11364 = 0%
    - SMTP: 0/11364 = 0%
    - POP3: 0/11364 = 0%
    - NTP: 0/11364 = 0%
    - unknown protocol on ports 5353->5353: 104/11364= 0.92%
    - unknown protocol on ports 17500->17500: 9/11364= 0.08%
    - unknown protocol on ports 57621->57621: 5/11364= 0.04%
    - unknown protocol on ports 1900->50880: 4/11364= 0.04%

4. www.etrigannews.com
    - HTTP: 0/30953 = 0%
    - HTTPS: 30758/30953 = 99.37%
    - DNS: 42/30953 = 0.14%
    - FTP: 0/30953 = 0%
    - SSH: 0/30953 = 0%
    - DHCP: 0/30953 = 0%
    - TELNET: 0/30953 = 0%
    - SMTP: 0/30953 = 0%
    - POP3: 0/30953 = 0%
    - NTP: 0/30953 = 0%
    - unknown protocol on ports 5353->5353: 122/30953= 0.39%
    - unknown protocol on ports 137->137: 18/30953= 0.06%
    - unknown protocol on ports 17500->17500: 8/30953= 0.03%
    - unknown protocol on ports 57621->57621: 5/30953= 0.02%

5. www.tmz.com
    - HTTP: 0/52801 = 0%
    - HTTPS: 52625/52801 = 99.67%
    - DNS: 160/52801 = 0.30%
    - FTP: 0/52801 = 0%
    - SSH: 0/52801 = 0%
    - DHCP: 0/52801 = 0%
    - TELNET: 0/52801 = 0%
    - SMTP: 0/52801 = 0%
    - POP3: 0/52801 = 0%
    - NTP: 12/52801 = 0.02%
    - unknown protocol on ports 5353->5353: 2/52801= 0.00%
    - unknown protocol on ports 1900->39589: 2/52801= 0.00%


```
www.youtube.com
 ---Ex3---
  https: 24494/24522= 99.89%
  dns: 28/24522= 0.11%

www.videolan.org
 ---Ex3---
  https: 2815/2831= 99.43%
  dns: 16/2831= 0.57%

www.joinpeertube.org
 ---Ex3---
  https: 11177/11364= 98.35%
  unknown protocol on ports 5353->5353: 104/11364= 0.92%
  dns: 65/11364= 0.57%
  unknown protocol on ports 17500->17500: 9/11364= 0.08%
  unknown protocol on ports 57621->57621: 5/11364= 0.04%
  unknown protocol on ports 1900->50880: 4/11364= 0.04%

www.etrigannews.com
 ---Ex3---
  https: 30758/30953= 99.37%
  unknown protocol on ports 5353->5353: 122/30953= 0.39%
  dns: 42/30953= 0.14%
  unknown protocol on ports 137->137: 18/30953= 0.06%
  unknown protocol on ports 17500->17500: 8/30953= 0.03%
  unknown protocol on ports 57621->57621: 5/30953= 0.02%

www.tmz.com
 ---Ex3---
  https: 52625/52801= 99.67%
  dns: 160/52801= 0.30%
  ntp: 12/52801= 0.02%
  unknown protocol on ports 5353->5353: 2/52801= 0.00%
  unknown protocol on ports 1900->39589: 2/52801= 0.00%

output from code/part1/pcap.py
```

### 1.5 Question #4
Report the number of unique destination IP addresses per site. Is there any
discernible difference between each site based on the number of destination IP
addresses? Do you see any direct relationship between number of destination IP
addresses and load time of the site?

There is not a massively huge discernible difference between each site based on the
number of destination IP addresses. It can be argued that websites which are more
image and video heavy may lead to additional requests to fetch such images/videos,
leading websites such as tmz and youtube to lead the pack in unique addresses.

In terms of page loading time and a direct relationship to the number of unique addresses
found, our calcultions for the PLTs came out to be quite not what we expected. It can be
argued however that even though websites like youtube have a larger number of unique
addresses and a lower PLT, this can be due to a caching system (even if we tried
to use a clean browser and to disable all caching), or the website having
more worldwide servers that ISPs have peered with to make load times faster on
popular websites.

| Link         | # of Unique Addresses | PLT       |
| -----------  | --------------------- | --------- |
| youtube      | 34                    | 3.53 sec  |
| videolan     | 26                    | 9.69 sec  |
| peertube     | 28                    | 7.92 sec  |
| etrigan      | 30                    | 11.69 sec |
| tmz          | 42                    | 9.35 sec  |

### 1.6 Question #5
List the top 5 destination IP addresses based on the number of packets sent. Can
you identify who owns these IP addresses?

1. www.youtube.com
    - 142.251.46.214 (i.ytimg.com): 67
    - 142.250.191.78 (www.youtube.com): 59
    - 198.189.66.47 (rr4---sn-jxopj-nh4e.googlevideo.com): 32
    - 198.189.66.45 (rr2---sn-jxopj-nh4e.googlevideo.com): 21
    - 142.251.46.225 (yt3.ggpht.com): 16

2. www.videolan.org
    - 213.36.253.2 (www.videolan.org, images.videolan.org): 347
    - 0.0.0.0 (www.google-analytics.com, get.videolan.org): 9
    - 172.64.100.2 (img.shields.io): 1

3. www.joinpeertube.org
    - 94.130.212.178 (joinpeertube.org): 171
    - 178.63.240.148 (framatube.org): 67
    - 176.9.199.8 (stats.framasoft.org): 5
    - 178.63.240.150 (instances.joinpeertube.org): 4

4. www.etrigannews.com
    - 54.204.238.15 (www.etrigannews.com): 136
    - 104.17.24.14 (cdnjs.cloudflare.com): 20
    - 0.0.0.0 (api.fouanalytics.com): 5
    - 69.16.175.42 (code.jquery.com): 5
    - 172.217.164.99 (fonts.gstatic.com): 5

5. www.tmz.com
    - 192.229.163.25 (platform.twitter.com): 195
    - 157.240.22.174 (www.instagram.com): 118
    - 142.250.189.206 (www.youtube-nocookie.com, www.youtube.com): 96
    - 13.227.74.105 (static.tmz.com): 64
    - 157.240.22.63 (scontent.cdninstagram.com): 55

Top **5** all together:
1. 213.36.253.2 (www.videolan.org, images.videolan.org): 347
2. 192.229.163.25 (platform.twitter.com): 195
3. 94.130.212.178 (joinpeertube.org): 171
4. 54.204.238.15 (www.etrigannews.com): 136
5. 157.240.22.174 (www.instagram.com): 118

```
www.youtube.com
---Ex5---
  142.251.46.214 (i.ytimg.com): 67
  142.250.191.78 (www.youtube.com): 59
  198.189.66.47 (rr4---sn-jxopj-nh4e.googlevideo.com): 32
  198.189.66.45 (rr2---sn-jxopj-nh4e.googlevideo.com): 21
  142.251.46.225 (yt3.ggpht.com): 16

www.videolan.org
 ---Ex5---
  213.36.253.2 (www.videolan.org, images.videolan.org): 347
  missing_IP_addr (www.google-analytics.com, get.videolan.org): 9
  172.64.100.2 (img.shields.io): 1

www.joinpeertube.org
 ---Ex5---
  94.130.212.178 (joinpeertube.org): 171
  178.63.240.148 (framatube.org): 67
  176.9.199.8 (stats.framasoft.org): 5
  178.63.240.150 (instances.joinpeertube.org): 4

www.etrigannews.com
 ---Ex5---
  54.204.238.15 (www.etrigannews.com): 136
  104.17.24.14 (cdnjs.cloudflare.com): 20
  missing_IP_addr (api.fouanalytics.com): 5
  69.16.175.42 (code.jquery.com): 5
  172.217.164.99 (fonts.gstatic.com): 5

www.tmz.com
 ---Ex5---
  192.229.163.25 (platform.twitter.com): 195
  157.240.22.174 (www.instagram.com): 118
  142.250.189.206 (www.youtube-nocookie.com, www.youtube.com): 96
  13.227.74.105 (static.tmz.com): 64
  157.240.22.63 (scontent.cdninstagram.com): 55

output from code/part1/pcap.py
```

### 1.7 Question #6
Is it possible that different IP addresses are mapped to the same hostname? Can
you find an example of this from the sites that you visited and explain why this
might be happening.

We did not observe this phenomena in the data that we collected, but we may assume
that this technique can be used for load balancing and/or for offering low-latency
connections by serving different geographical regions with different servers (due
to transfer delay).
