import dpkt
import socket
import glob   #For opening files
from typing import Dict

# I define constants for the protocols to analyze
PROTOCOLS = ["http", "https", "ftp", "ssh", "telnet", "smtp", "pop3", "ntp", "dns", "dhcp"]
PROTOCOL_CACHE = {socket.getservbyname(i): i for i in PROTOCOLS[:-2]}

# IDK why dns and dhcp are not mapped by socket API
PROTOCOL_CACHE[53] = "dns"
PROTOCOL_CACHE[67] = "dhcp" # server
PROTOCOL_CACHE[68] = "dhcp" # client

if __name__ == "__main__":

    print("Welcome, this scripts takes a .pcap file (or a directory of .pcap files) and analyzes it")
    # filename = input("Enter the path for the file you want to analyze: ")
    filename = "./../../captures/part1_pcaps"

    if filename[-5:] == ".pcap":
        files = [filename]
    else:
        files = glob.glob(f"{filename}/*.pcap")

    for file in files:

        print(file.split('/')[-1])

        # Counters for transmission and application layer protocols
        trans_counts: Dict = {}
        app_counts: Dict = {}

        # Counters for destination IP address
        ip_dest_counts: Dict = {}

        # Counters for TCP and UDP packets
        udp_counter = 0
        tcp_counter = 0

        with open(file, "rb") as f:
            pcap = dpkt.pcap.Reader(f)

            # Loop through all packets
            for ts, buf in pcap:
                eth = dpkt.ethernet.Ethernet(buf)
                ip = eth.data

                # Filtering on network layer for only IP traffic
                if eth.type==dpkt.ethernet.ETH_TYPE_IP:

                    # Increasing counter for destination address
                    ip_dest_counts[ip.dst] = ip_dest_counts.get(ip.dst, 0) + 1

                    # Filtering on transmission layer for TCP traffic
                    if ip.p==dpkt.ip.IP_PROTO_TCP:
                        tcp=ip.data

                        tcp_counter+=1

                        if tcp.dport in PROTOCOL_CACHE:
                            protocol_name = PROTOCOL_CACHE[tcp.dport]
                        elif tcp.sport in PROTOCOL_CACHE:
                            protocol_name = PROTOCOL_CACHE[tcp.sport]
                        else:
                            protocol_name = f"unknown protocol on ports {tcp.dport}->{tcp.sport}"

                        # Increase count
                        app_counts[protocol_name] = app_counts.get(protocol_name, 0) + 1

                    # Filtering on transmission layer for UDP traffic
                    elif ip.p==dpkt.ip.IP_PROTO_UDP:
                        udp=ip.data

                        udp_counter+=1

                        if udp.dport in PROTOCOL_CACHE:
                            protocol_name = PROTOCOL_CACHE[udp.dport]
                        elif udp.sport in PROTOCOL_CACHE:
                            protocol_name = PROTOCOL_CACHE[udp.sport]
                        else:
                            protocol_name = f"unknown protocol on ports {udp.dport}->{udp.sport}"

                        # Increase count
                        app_counts[protocol_name] = app_counts.get(protocol_name, 0) + 1

                    # Detecting and counting transmission layer protocols
                    protocol_name = ip.get_proto(ip.p).__name__
                    trans_counts[protocol_name] = trans_counts.get(protocol_name, 0) + 1

        # Ex 1 solution
        print("\n ---Ex1---")
        print(f"  UDP: {udp_counter}, TCP: {tcp_counter}")

        # Ex 2 solution
        print("\n ---Ex2---")
        n_http = app_counts['http'] if 'http' in app_counts else 0
        n_https = app_counts['https'] if 'https' in app_counts else 0
        print(f"  HTTP: {n_http}, HTTPS: {n_https}")

        # # Ex 3 solution
        print("\n ---Ex3---")
        tot = sum(v for v in app_counts.values())
        for v, k in sorted(zip(app_counts.values(), app_counts.keys()), reverse=True):
            print(f"  {k}: {v}/{tot}= {v/tot*100:.2f}%")

        # # Ex 4/5 solution
        print("\n ---Ex(4,5)---")
        print(f" {len(ip_dest_counts)} different destination IP addresses")
        for v, k in sorted(zip(ip_dest_counts.values(), ip_dest_counts.keys()), reverse=True):
            print(f"  {socket.inet_ntoa(k)}: {v}")

        print()

