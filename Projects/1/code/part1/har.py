import socket
import glob   #For opening files
from typing import Dict


if __name__ == "__main__":

    # Install haralyzer (https://github.com/haralyzer/haralyzer) if missing
    import subprocess
    import sys
    subprocess.check_call([sys.executable, "-m", "pip", "install", "haralyzer"], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    import haralyzer

    print("Welcome, this scripts takes a .har file (or a directory of .har files) analyzes it")
    # filename = input("Enter the path for the file you want to analyze: ")
    filename = "../../captures/part1_hars"
    print()

    if filename[-4:] == ".har":
        files = [filename]
    else:
        files = glob.glob(f"{filename}/*.har")

    for file in files:

        print(file.split('/')[-1])

        har_parser = haralyzer.HarParser.from_file(file)

        reqs = [0, 0] #HTTP, HTTPS

        ip_counts: Dict = {}

        hostname_to_ip: Dict = {}

        # Total page(s) load time
        tot_PLT = 0

        # Loop through pages
        for i in har_parser.pages:

            try:
                tot_PLT += i.content_load_time
            except:
                pass

            # Loop through requests
            for j in i.get_requests:

                ip_addr = j.serverAddress
                hostname = j.url.split('/')[2]

                if len(ip_addr) == 0:
                    if hostname in hostname_to_ip:
                        ip_addr = next(iter(hostname_to_ip[hostname]))
                    else:
                        ip_addr = "missing_IP_addr" #socket.gethostbyname(hostname)?

                hostname_to_ip[hostname] = set.union(hostname_to_ip.get(hostname, set()), set([ip_addr]))

                if not ip_addr in ip_counts:
                    ip_counts[ip_addr] = {hostname: 1}
                else:
                    ip_counts[ip_addr][hostname] = ip_counts[ip_addr].get(hostname, 0) + 1

                # Loop through headers in request
                for k in j.request.headers:

                    # Detecting the protocol used
                    if k["name"] == ":scheme":
                        if k["value"] == "http":
                            reqs[0] += 1
                        elif k["value"] == "https":
                            reqs[1] += 1
                        else:
                            raise BaseException("neither http nor https")
                        break

        # Ex 2 solution
        print("\n ---Ex2---")
        print(f"  HTTP reqs: {reqs[0]}, HTTPS reqs: {reqs[1]}")

        # Ex 4 PLT
        print("\n ---Ex4---")
        print(f"  {tot_PLT/1000:.2f} sec")

        # Ex 5 solution
        print("\n ---Ex5---")
        #DGB#for ip_counts, ip_addr in zip(ip_counts.values(), ip_counts.keys()):
        #DGB#    print(f"  {ip_addr}:")
        #DGB#    for hostname, count in zip(ip_counts.keys(), ip_counts.values()):
        #DGB#        print(f"    {hostname}: {count}")
        for n_requests_per_IP, ip_addr, hostnames_per_IP in sorted(zip([sum(i.values()) for i in ip_counts.values()], ip_counts.keys(), [list(i.keys()) for i in ip_counts.values()]), reverse=True)[:5]:
            print(f"  {ip_addr} ({', '.join(i for i in hostnames_per_IP)}): {n_requests_per_IP}")

        # Ex 6 solution
        print("\n ---Ex6---")
        for hostname, ip_list in zip(hostname_to_ip.keys(), hostname_to_ip.values()):
            if len(ip_list)>1:
                print(f"  {hostname}: {', '.join(str(i) for i in unique_hostnames[hostname])}")

        print()







