import dpkt
import socket
import binascii  #To decode bytes
import glob      #To read files
from typing import Dict
# DBG # import plotly.express as px
# DBG # COUNTER_DBG = 20000

# Define constants
PROTOCOLS = ["http", "https", "ftp", "ssh", "telnet", "smtp", "pop3", "ntp", "dns", "dhcp"]
PROTOCOL_CACHE = {socket.getservbyname(i): i for i in PROTOCOLS[:-2]}

# IDK why dns and dhcp are not mapped by socket api
PROTOCOL_CACHE[53] = "dns"
PROTOCOL_CACHE[67] = "dhcp" # server
PROTOCOL_CACHE[68] = "dhcp" # client

def sec_to_minsec(secs: int):
    return f"{int(secs//60)}:{int(secs%60):02d} sec"

if __name__ == "__main__":
    print("Welcome, this scripts takes a .pcap file (or a directroy of .pcap files) and analyzes it")
    # filename = input("Enter the path for the file you want to analyze: ")
    filename = "./../../captures/project1_part2.pcap"

    if filename[-5:] != ".pcap":
        print("Invalid filename")
        raise Exception

    # Record of IP communication. Format: (timestamp, srcIP, dstIP, protocol_name)
    ip_log = []

    # Record of ARP responses. Format: (timestamp, MAC, IP)
    arp_log = []

    # Counter of packets per application layer protocol
    app_proto_counts: Dict = {}

    # Counter of packets sent per ip
    dst_ip_pack_count: Dict = {}

    # Counterof packets recieved per ip
    src_ip_pack_count: Dict = {}

    # Counters for TCP and UDP packets
    udp_counter = 0
    tcp_counter = 0

    start_ts = float("inf")

    # Open the .pcap file and gather data
    with open(filename, "rb") as f:
        pcap = dpkt.pcap.Reader(f)

        # Loop through packets
        for ts, buf in pcap:
            start_ts = min(start_ts, ts)

            eth = dpkt.ethernet.Ethernet(buf)
            ip = eth.data

            protocol_name = ""

            # ARP packages
            if eth.type==dpkt.ethernet.ETH_TYPE_ARP:
                # op==2 is a reply
                if ip.op == 2:
                    # ip.pprint()
                    ip_str = socket.inet_ntoa(ip.spa)
                    mac = binascii.hexlify(ip.sha).decode("utf-8")
                    arp_log.append((ts-start_ts, mac, ip_str))
                    # print(f"MAC: {mac}, IP: {ip_str}")

            # Filter on network layer for IP packets
            if eth.type in (dpkt.ethernet.ETH_TYPE_IP, eth.type==dpkt.ethernet.ETH_TYPE_IP6):

                if ip.v==6:
                    ip_src = socket.inet_ntop(socket.AF_INET6, ip.src)
                    ip_dst = socket.inet_ntop(socket.AF_INET6, ip.dst)

                elif ip.v==4:
                    ip_src = socket.inet_ntoa(ip.src)
                    ip_dst = socket.inet_ntoa(ip.dst)

                # Increasing counter for destination address
                dst_ip_pack_count[ip_dst] = dst_ip_pack_count.get(ip_dst, 0) + 1
                src_ip_pack_count[ip_src] = src_ip_pack_count.get(ip_src, 0) + 1

                # Filter on transport layer for TCP packets
                if ip.p==dpkt.ip.IP_PROTO_TCP:
                    tcp=ip.data

                    tcp_counter+=1

                    if tcp.dport in PROTOCOL_CACHE:
                        protocol_name = PROTOCOL_CACHE[tcp.dport]
                    elif tcp.sport in PROTOCOL_CACHE:
                        protocol_name = PROTOCOL_CACHE[tcp.sport]
                    else:
                        protocol_name = f"unknown protocol on ports {tcp.dport}->{tcp.sport}"

                # Filter on transport layer for UDP packets
                elif ip.p==dpkt.ip.IP_PROTO_UDP:
                    udp=ip.data

                    udp_counter+=1

                    if udp.dport in PROTOCOL_CACHE:
                        protocol_name = PROTOCOL_CACHE[udp.dport]
                    elif udp.sport in PROTOCOL_CACHE:
                        protocol_name = PROTOCOL_CACHE[udp.sport]
                    else:
                        protocol_name = f"unknown protocol on ports {udp.dport}->{udp.sport}"

                app_proto_counts[protocol_name] = app_proto_counts.get(protocol_name, 0) + 1

                ip_log.append((ts-start_ts, ip_src, ip_dst, protocol_name))

                # DBG # COUNTER_DBG -= 1
                # DBG # if COUNTER_DBG == 0:
                # DBG #     break

    # Useful variables
    ips = set(i for i in tuple(src_ip_pack_count.keys())+tuple(dst_ip_pack_count.keys()))
    local_ips = set(i for i in ips if (i.split(".")[:-1]==["10","42","0"] and (i.split(".")[-1]!="1")))
    endpoints = ips - local_ips

    # # Ex 1 solution
    print("\n ---Ex1---")
    print("  " + str(len(local_ips)))

    # # Ex 2 solution
    print("\n ---Ex2---")
    print("  " + max((src_ip_pack_count[ip], ip) for ip in local_ips)[1])

    # # Ex 3 solution
    print("\n ---Ex3---")
    print("  " + max((dst_ip_pack_count[ip], ip) for ip in local_ips)[1])

    # # Ex 4 solution
    print("\n ---Ex4---")
    endpoints_ips_req_sources: Dict = {}
    for req in ip_log:
        dst = req[1]
        src = req[2]
        protocol = req[3]
        if src in local_ips and dst in endpoints:
            endpoints_ips_req_sources[dst] = set.union(endpoints_ips_req_sources.get(dst, set()), set([src]))
    print('\n'.join('  ' + dst for dst, sources in zip(endpoints_ips_req_sources.keys(), endpoints_ips_req_sources.values()) if len(sources)>1))

    # # Ex 5 solution
    print("\n ---Ex5---")
    print(''.join(f"  {i[1]}: {i[0]}\n" for i in sorted(zip(app_proto_counts.values(), app_proto_counts.keys()), reverse=True)[:3]))

    # # Ex 6 solution
    print("\n ---Ex6---")
    tot_seconds = max(i[0] for i in ip_log) - min(i[0] for i in ip_log)
    print("  " + sec_to_minsec(tot_seconds))


    # # Ex 7 solution
    print("\n ---Ex7---")
    # Devices that communicate at the same exact timestamp, might change this
    # DBG # devices_per_time = ([], [])
    concurrent_ips = set()
    last_ts = 0
    unique_ips_in_ts = set()
    for req in ip_log:
        ts = req[0]
        src = req[1]
        dst = req[2]
        for endpoint in (src, dst):
            if endpoint in local_ips:
                if ts != last_ts:
                    # DBG # devices_per_time[0].append(tuple(unique_ips_in_ts))
                    # DBG # devices_per_time[1].append(last_ts)
                    if len(unique_ips_in_ts)>1:
                        concurrent_ips.add(endpoint)
                    unique_ips_in_ts.clear()

                unique_ips_in_ts.add(endpoint)
                last_ts = ts

    # DBG # print([i for i in devices_per_time[0] if len(i)>=2])
    # DBG # px.scatter(x=devices_per_time[1], y=[len(i) for i in devices_per_time[0]]).show()
    # DBG # times = []
    # DBG # ips = []
    # DBG # for req in ip_log:
    # DBG #     if req[1] in local_ips:
    # DBG #         times.append(req[0])
    # DBG #         ips.append(req[1])
    # DBG #     if req[2] in local_ips:
    # DBG #         times.append(req[0])
    # DBG #         ips.append(req[2])
    # DBG # px.scatter(x=times, y=ips).show()

    print('  '+', '.join(ip for ip in concurrent_ips))


    # # Ex 8 solution
    print("\n ---Ex8---")
    # I save the last IP request for every device
    ips_last_reqest_ts: Dict = {ip:0 for ip in local_ips}
    for req in ip_log:
        ts = req[0]
        src = req[1]
        if src in local_ips:
            ips_last_reqest_ts[src] = ts

    print(" Last IP request sent:")
    print('\n'.join("  {}: {}".format(ip, sec_to_minsec(ts)) for ts, ip in sorted(zip(ips_last_reqest_ts.values(), ips_last_reqest_ts.keys()))))

    # I save the last ARP request for every device
    macs_last_reqest_ts: Dict = {record[1]:(0,record[2]) for record in arp_log if record[2] in local_ips}
    for req in arp_log:
        ts = req[0]
        mac = req[1]
        ip = req[2]
        if ip in local_ips:
            macs_last_reqest_ts[mac] = (ts, ip)

    print(" Last ARP response sent:")
    print('\n'.join("  {}<=>{}: {}".format(mac, ip, sec_to_minsec(ts)) for (ts, ip), mac in sorted(zip(macs_last_reqest_ts.values(), macs_last_reqest_ts.keys()))))


    print()


