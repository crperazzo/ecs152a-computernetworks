from __future__ import annotations
import socket
import struct

class TCPsocket:

    sock: socket.socket

    src_port: int
    src_addr: str
    dst_port: int
    dst_addr: str

    bound: bool
    listening: bool

    def __init__(self, port_number:int = 0) -> None:
        self.bound = False
        self.listening = False
        self.sock = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)

    def set_timeout(self, timeout:int) -> None:
        ...

    def bind(self, src_addr_port: tuple[str, int]) -> None:
        self.src_addr = src_addr_port[0]
        self.src_port = src_addr_port[1]
        self.bound = True

    def close(self) -> None:
        """TODO: add FIN/ACK stuf"""
        self.sock.close()

    def getsockname(self) -> tuple[str, int]:
        return (self.src_addr, self.src_port)

    # Virtual methods
    def connect(self, other_address_port: tuple[str, int]) -> None:
        raise NotImplementedError("Tried to call a pure virtual method")

    def accept(self) -> TCPsocket:
        raise NotImplementedError("Tried to call a pure virtual method")

    def listen(self) -> None:
        raise NotImplementedError("Tried to call a pure virtual method")

    def recv(self) -> bytes:
        raise NotImplementedError("Tried to call a pure virtual method")

    def sendall(self, bytestring: bytes) -> None:
        raise NotImplementedError("Tried to call a pure virtual method")


