from __future__ import annotations
from TCP import TCPsocket
from types import MappingProxyType
from utilities import pack_bins_to_byte, unpack_bytes_to_bin
import socket
import struct

DEBUG = True

class TCPsocketPutah(TCPsocket):

    # Format: {Name: n_bits}
    header_bit_format = MappingProxyType({
                            "_": 5, "SYN": 1, "ACK": 1, "FIN": 1, #1st byte (padding 0s to the left AKA most significant)
                            "SRC_PORT": 16, #2nd+3rd byte
                        })

    header_struct_format = "!cH"


    @staticmethod
    def construct_header(SYN:bool=False, ACK:bool=False, FIN:bool=False, SRC_PORT:int=0) -> bytes:
        return pack_bins_to_byte([SYN, ACK, FIN], DEBUG=DEBUG) + struct.pack("!H", SRC_PORT)

    @staticmethod
    def get_header_props(msg: bytes) -> dict[str, int]:
        offset_bits = 0
        to_ret: dict[str, int] = {}

        #I assume padding to bytes
        for name, length in TCPsocketPutah.header_bit_format.items():
            offset_bits += length
            if length < 8:
                to_ret[name] = unpack_bytes_to_bin(msg[offset_bits//8 : offset_bits//8+1])[offset_bits%8]
            else:
                to_ret[name] = int.from_bytes(msg[offset_bits//8 : offset_bits//8 + length//8], "big")

        return to_ret


    def connect(self, server_welcome_addr_port: tuple[str, int]) -> None:
        """
        Connect to a server that is listening
        This function can only be used on the client side
        """

        # First I create a temporary UDP socket to do the handshake
        temp_sock = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
        temp_sock.bind(('127.0.0.1', 0)) #let the OS assign a free port

        # Bind the data socket to get a free port
        self.bind(('127.0.0.1', 0))
        src_data_port = self.getsockname()[-1]

        SYNmsg = self.construct_header(SYN=True, SRC_PORT=src_data_port)

        ACK_recieved = False
        while not ACK_recieved:

            # Send SYN
            bytes_sent = 0
            while bytes_sent < len(SYNmsg):
                print(f"Sent SYN to {server_welcome_addr_port}")
                bytes_sent += temp_sock.sendto(SYNmsg, server_welcome_addr_port)

            # Receive ACK
            try:
                ACKmsg, (dst_addr, dst_port) = temp_sock.recvfrom(len(SYNmsg)) #SYN, SYN/ACK, ACK and FIN have the same lenght (pure header)

                # Keep recieving while getting unwanted packets
                while dst_addr != dst_addr and dst_port != self.dst_port:
                    ACKmsg, (dst_addr, dst_port) = temp_sock.recvfrom(len(SYNmsg)) #SYN, SYN/ACK, ACK and FIN have the same lenght (pure header)
                    print("Recieved unwanted shit")

                header_props = self.get_header_props(ACKmsg)
                if header_props["ACK"] == True:
                    dst_data_port = header_props["SRC_PORT"]
                    ACK_recieved = True

            except socket.timeout:
                if DEBUG:
                    print("Timeout on ACK")

        print(f"Connected to {dst_addr}:{dst_data_port}")

        temp_sock.close()



    def accept(self) -> TCPsocketPutah:
        """
        Accept a connection by a client who uses connect()
        This function can be used only by the server and on the welcome socket
        :returns the socket on which data communication will happen
        """
        if not self.listening:
            raise RuntimeError("Cannot accept connection if server is not listening")

        print(f"Listening for connections on {self.getsockname()}")

        # Construct the header (incomplete, I still need SRC_PORT)
        SYN_ACK_msg = self.construct_header(SYN=True, ACK=True)

        # Recieve a SYN message
        SYN_recieved = False
        while not SYN_recieved:

            SYN_msg, (dst_addr, dst_port_welcome) = self.sock.recvfrom(len(SYN_ACK_msg))

            print("Server recieved something")

            SYN_props = self.get_header_props(SYN_msg)

            if SYN_props["SYN"] == True:
                SYN_recieved = True

        dst_data_port = SYN_props["SRC_PORT"]

        # Create data socket and bind it
        data_sock = TCPsocketPutah()
        data_sock.sock = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
        data_sock.dst_port = dst_data_port
        data_sock.dst_addr = dst_addr
        data_sock.bind((dst_addr, 0))
        data_port = data_sock.getsockname()[-1]

        SYN_ACK_msg = self.construct_header(SYN=True, ACK=True, SRC_PORT=data_port)

        self.sock.sendto(SYN_ACK_msg, (dst_addr, dst_port_welcome))

        if DEBUG:
            print(f"Accepted host {dst_addr}:{dst_data_port}")

        return data_sock


    def listen(self) -> None:
        """
        Start listening to incoming connections
        This function can be used only by the server
        """
        self.listening = True

    def recv(self) -> bytes:
        ...

    def send(self) -> None:
        ...
