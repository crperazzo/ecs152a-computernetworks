import socket
from TCPputah import TCPsocketPutah

# msgFromClient = "Hello UDP Server"
# bytesToSend = str.encode(msgFromClient)
# serverAddressPort = ("127.0.0.1", 20001)
# bufferSize = 1024

# # Create a UDP socket at client side
# UDPClientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)

# # Send to server using created UDP socket
# UDPClientSocket.sendto(bytesToSend, serverAddressPort)

# msgFromServer = UDPClientSocket.recvfrom(bufferSize)

# msg = "Message from Server {}".format(msgFromServer[0])

# print(msg)


server_ip = "127.0.0.1"
server_welcome_port = 42069

welcome_sock = TCPsocketPutah()
welcome_sock.bind(("127.0.0.1", 0))
welcome_sock.connect((server_ip, server_welcome_port))
