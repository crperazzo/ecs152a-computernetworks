from __future__ import annotations

def pack_bins_to_byte(eights_bits: list[bool], DEBUG=False) -> bytes:
    """
    :returns a bytestring, with length = ceil(eights_bits/8)
    """
    l = len(eights_bits)
    if DEBUG and l%8 != 0:
        print("Warning: triying to pack n_bits%8 != 0")
    return int.to_bytes(sum(bit << i for bit, i in zip(eights_bits, reversed(range(l)))), (l+7)//8, "big")

def unpack_bytes_to_bin(bytestring: bytes, DEBUG=False) -> list[bool]:
    """
    :returns a list of booleans containing 8*len(bytestring) elements
    """
    if DEBUG and len(bytestring)==0:
        print("Warning: trying to unpack an empty bytestring")
    bin_str = bin(int.from_bytes(bytestring, "big"))[2:]
    n_bits_to_reach_8k = (8 - (len(bin_str)%8))%8
    return [False]*n_bits_to_reach_8k + [bool(int(bit)) for bit in bin_str]
